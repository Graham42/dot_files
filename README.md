# My Automated Config Setup

I wanted an automated way to quickly setup a shell with my own customizations.  Setup is at ~3
commands right now.

Setup steps:

```sh
git clone https://github.com/Graham42/dot_files.git ~/dot_files
~/dot_files/init_all.sh
source ~/.bashrc
```

# Structure

- Anything in `home/` is linked to the user's home directory and then the directory structure
  inside is followed
- All dot files in `home/` are escaped with a leading underscore so it's not as easy to forget
  about files that are hidden. Symbolic links are created with the leading underscore's removed.
  Example: `_.vimrc` becomes `.vimrc`
- Running `init_all.sh` will create and update a marked section in `~/.bashrc`.
  - `bashrc_body.sh` is the main block content.
  - If there are files in the `os_specific_bashrc/` folder, they will be included if the OS check
    passes.  OS specific files require a 2 line header, where the second line is a commented shell
    command that will return a zero exit status if it detects the correct OS.

    Example:

    ```sh
    # OS check:
    # cat /etc/*-release | grep '^NAME=\"Arch Linux\"' -q
    ```
- Any scripts in `setup_scripts` will be run, but they must be executable or the setup will fail.


# Docker Image

The Docker image of the repo is intended for use as a lightweight development environment in Windows
when combined with boot2docker.

This docker image can be created with:

```sh
docker run --name=dev -it graham42/dev-env /bin/bash
```

For access the next times:

```sh
# make sure container is started
docker start dev
# Get a shell where tmux works
# See this open bug: https://github.com/docker/docker/issues/8755
docker exec -it dev script /dev/null -c /bin/bash
```


# License

MIT License

Copyright (c) 2016 Graham McGregor

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
